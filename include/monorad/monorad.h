/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <vector>

#include "ros/ros.h"
#include "delphi_esr_msgs/EsrEthTx.h"
#include "sensor_msgs/CompressedImage.h"
#include "sensor_msgs/CameraInfo.h"
#include "std_msgs/Header.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include "eigen3/Eigen/Core"

#include "monorad/radar_detection.h"
#include "monorad/trans_vel_estimator.h"
#include "monorad/rot_vel_estimator.h"

/**
 * @brief Class for the "MonoRAD" ego-motion estimator that estimates sensor ego-motion from
 *        monocular camera images and data from a single RADAR. See "MonoRAD: Monocular Camera- 
 *        and RADAR-based Ego-Motion Estimation" (Monaco et al., 2019)
 */
class MonoRAD {
 public:
  MonoRAD();

 private:
  /**
   * @brief Callback whenever the RADAR publishes data from its mid- or long-range scans
   * 
   * @param[in] radar_data_msg_ptr    Pointer for message from Delphi ESR 2.5 RADAR 
   *                                    as provided by AutonomouStuff ROS node
   *                                    ("Raw", unfiltered data via Ethernet)
   */
  void RadarDataCallback(const delphi_esr_msgs::EsrEthTxConstPtr radar_data_msg_ptr);

  /**
   * @brief Stores RADAR data and checks if both scans have been stored
   * 
   * @param[in] radar_data_msg    Radar data message from callback for mid or long range scan
   * 
   * @return true                 If both mid and long range scans have been stored for this cycle
   * @return false                Otherwise
   */
  bool StoreRadarData(const delphi_esr_msgs::EsrEthTx& radar_data_msg);

  /**
   * @brief Callback whenever the monocular camera publishes its info (intrinsic calibration
   *        parameters)
   * 
   * @param[in] camera_info_msg_ptr    Pointer for ROS sensor_msgs CameraInfo message
   */
  void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr camera_info_msg_ptr);

  /**
   * @brief Callback whenever the monocular camera publishes its (compressed) image
   * 
   * @param[in] image_msg_ptr    Pointer for ROS sensor_msgs CompressedImage message
   */
  void CameraImageCallback(const sensor_msgs::CompressedImageConstPtr image_msg_ptr);

  /**
   * @brief Publishes the RADAR's translational ego-motion estimate. Note that this function
   *        is overloaded for both the RADAR's and camera's estimates. The utilized RADAR
   *        does not measure elevation angles, so it cannot publish its v_z. Thus, the vector
   *        of its estimate only has two components.
   * 
   * @param[in] radar_trans_vel_estimate             Two-element vector of the RADAR's translational
   *                                                 ego-motion estimate in the RADAR's coordinate
   *                                                 system (using robotic vehicle coordinate
   *                                                 system conventions)
   * @param[in] radar_trans_vel_covariance_matrix    2x2 covariance matrix of the aforementioned
   *                                                 estimate
   * @param[in] msg_header                           ROS message header of the source RADAR message
   */
  void PublishEgomotionEstimate(
      const Eigen::Vector2d& radar_trans_vel_estimate,
      const Eigen::Matrix<double, 2, 2>& radar_trans_vel_covariance_matrix,
      const std_msgs::Header& msg_header);

  /**
   * @brief Publishes the camera's rotational ego-motion estimate. Note that this function
   *        is overloaded for both the RADAR's and camera's estimates.
   *
   * @param[in] camera_rot_vel_estimate_body_coord   Three-element vector of the camera's
   *                                                 rotational ego-motion estimate in the camera's
   *                                                 coodinate system (but using robotic vehicle
   *                                                 coordinate system conventions)
   * @param[in] msg_header                           ROS message header of the source RADAR message
   */
  void PublishEgomotionEstimate(
      const Eigen::Vector3d& camera_rot_vel_estimate_body_coord,
      const std_msgs::Header& msg_header);

  ros::NodeHandle nh_;                                     // ROS nodehandle
  ros::Subscriber radar_data_subscriber_;                  // ROS RADAR data subscriber
  ros::Subscriber camera_info_subscriber_;                 // ROS camera info subscriber
  ros::Subscriber camera_image_subscriber_;                // ROS camera image subscriber
  ros::Publisher radar_egomotion_publisher_;               // RADAR ego-motion estimate ROS
                                                           //   publisher
  ros::Publisher camera_egomotion_publisher_;              // Camera ego-motion estimate ROS
                                                           //   publisher
  geometry_msgs::TwistWithCovarianceStamped radar_egomotion_msg_;
                                                           // Timestamped ROS RADAR ego-motion
                                                           //   estimate message with covariance
                                                           //   matrix
  geometry_msgs::TwistWithCovarianceStamped camera_egomotion_msg_;
                                                           // Timestamped ROS camera ego-motion
                                                           //   estimate message with covariance
                                                           //   matrix
  bool mid_range_scan_stored_;                             // True if RADAR measurement's mid-range
                                                           //   scan was stored
  bool long_range_scan_stored_;                            // True if RADAR measurement's long-
                                                           //   range scan was stored
  std::vector<RadarDetection> valid_radar_detections_;     // Vector of RadarDetection objects used
                                                           //   to store a RADAR measurement's valid
                                                           //   mid- and long-range RADAR targets
  Eigen::Matrix<double, 3, 3> body_coord_R_camera_coord_;  // 3x3 rotation matrix that describes
                                                           //   the rotation from the robotic's
                                                           //   vehicle body coord. system to the
                                                           //   camera'coordinate system. It is
                                                           //   general, i.e. does not represent
                                                           //   the extrinsics of any particular
                                                           //   platform.
  TransVelEstimator trans_vel_estimator_;                  // Object instance for the RADAR-based
                                                           //   translational velocity estimator
                                                           //   class (TransVelEstimator)
  RotVelEstimator rot_vel_estimator_;                      // Object instance for the camera-based
                                                           //   rotational velocity estimator
                                                           //   class (RotVelEstimator)
};
