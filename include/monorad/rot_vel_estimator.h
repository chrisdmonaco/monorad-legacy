/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <deque>
#include <vector>

#include "eigen3/Eigen/Core"
#include "opencv2/core/core.hpp"

/**
 * @brief Class that estimates the monocular camera's rotational ego-motion from its images
 * 
 */
class RotVelEstimator {
 public:
  /**
   * @brief Construct a new RotVelEstimator object
   * 
   * @param[in] max_num_features                // Maximum number of image corner features to 
   *                                            //   extract
   * @param[in] min_feature_pixel_distance      // Minimum distance [pixels] between extracted 
   *                                            //   features
   * @param[in] min_feature_quality_level       // Minimum quality level of extracted features [pu]
   *                                            //   See cv::goodFeaturesToTrack(...)
   * @param[in] num_image_row_sections          // Number of image section rows
   * @param[in] num_image_col_sections          // Number of image section columns
   * @param[in] visible_hood_edge_top_v         // Image coordinate (v) that marks the (image) top
   *                                            //   of the visible vehicle hood
   * @param[in] visible_hood_edge_side_v        // Image coordinate (v) that marks the location 
   *                                            //   that the side of the vehicle hood leaves the 
   *                                            //   image
   * @param[in] essential_matrix_probability    // Desired level of confidence (probability)
   *                                            //   that the essential matrix is correct. See
   *                                            //   cv::findEssentialMat(...)
   * @param[in] essential_matrix_ransac_pixel_threshold 
   *                                            // Threshold distance [pixels] between feature and
   *                                            //   its estimated epipolar line to be considered a
   *                                            //   static inlier
   * @param[in] min_num_of_tracks               // Minimum number of feature tracks required to 
   *                                            //   confidently report an ego-motion estimate
   */
  RotVelEstimator(const uint max_num_features,
                  const uint min_feature_pixel_distance,
                  const double min_feature_quality_level,
                  const uint num_image_row_sections,
                  const uint num_image_col_sections,
                  const uint visible_hood_edge_top_v,
                  const uint visible_hood_edge_side_v,
                  const double essential_matrix_probability,
                  const double essential_matrix_ransac_pixel_threshold,
                  const uint min_num_of_tracks);

  /**
   * @brief Stores intrinsic camera parameters as provided by the CameraInfo message. Also
   *        calculates and stores constant parameters based on these intrinsic parameters.
   * 
   * @param[in] fx             // Camera focal length [pixels] corresponding to camera's x-axis
   * @param[in] fy             // Camera focal length [pixels] corresponding to camera's y-axis
   * @param[in] cx             // Image u-coordinate of the principal point (camera's x-axis)
   * @param[in] cy             // Image v-coordinate of the principal point (camera's y-axis)
   * @param[in] image_width    // Image width
   * @param[in] image_height   // Image height
   */
  void SetCameraInfo(const double fx,
                     const double fy,
                     const double cx,
                     const double cy,
                     const uint image_width,
                     const uint image_height);

  /**
   * @brief Estimates the monocular camera's rotational ego-motion from its images
   * 
   * @param[in] grayscale_image          // Input grayscale image [CV_8UC1]
   * @param[in] timestamp                // Image timestamp [sec.] from image message
   * @param[in] seq_id                   // Sequence ID from image message
   *                                     //   (optionally used for marking processed images)
   * @param[out] rot_vel_camera_coord    // 3x1 vector of the camera's rotational ego-motion
   *                                     //   using the conventions of the "camera" coordinate
   *                                     //   system
   * @return true                        // If a valid estimate has been calculated
   * @return false                       // Otherwise
   */
  bool Estimate(const cv::Mat& grayscale_image,
                const double timestamp,
                const uint seq_id,
                Eigen::Matrix<double, 3, 1>& rot_vel_camera_coord);

 private:
  /**
   * @brief Extract so-called "good features to track" image corner features from
   *        a grayscale image
   * 
   * @param[in] grayscale_image         // Grayscale image [CV_8UC1]
   * @param[out] extracted_features     // Vector of extracted corner features (points)
   */
  void ExtractFeatures(const cv::Mat& grayscale_image,
                       std::vector<cv::Point2f>& extracted_features);

  /**
   * @brief Tracks how detected image feautures moved from the previous frame to the current frame
   * 
   * @param[in] previous_grayscale_image      // Previous grayscale image [CV_8UC1]
   * @param[in] current_grayscale_image       // Current grayscale image [CV_8UC1]
   * @param[in] previous_detected_features    // Vector of previous image's detected corner 
   *                                          //   features (Caution: function modifies this 
   *                                          //   input)
   * @param[out] previous_tracked_features    // Vector of previous image's detected corner 
   *                                          //   features that were successfully tracked in 
   *                                          //   the current frame
   * @param[out] current_tracked_features     // Vector of current image locations of features 
   *                                          //   tracked from the previous frame
   */
  void TrackFeatures(const cv::Mat& previous_grayscale_image,
                     const cv::Mat& current_grayscale_image,
                     std::vector<cv::Point2f>& previous_detected_features,
                     std::vector<cv::Point2f>& previous_tracked_features,
                     std::vector<cv::Point2f>& current_tracked_features);

  /**
   * @brief Estimates camera rotation from tracked features between the previous and current image.
   *        Does so via essential matrix estimation (and subsequent decomposition).
   * 
   * @param[in] previous_tracked_features        // Vector of previous image's detected corner 
   *                                             //   features that were successfully tracked in 
   *                                             //   the current frame
   * @param[in] current_tracked_features         // Vector of current image locations of features 
   *                                             //   tracked from the previous frame
   * @param[out] essential_matrix_corner_mask    // "Boolean" [CV_8UC1] mask that designates which
   *                                             //   tracked feature pairs were considered inliers
   *                                             //   for the essential matrix calculation
   * @param[out] rotation_camera_coord           // 3x1 vector of the camera's rotation [rad.] in
   *                                             //   the camera's coordinate system using camera
   *                                             //   camera coordinate system conventions
   */
  void EstimateRotation(const std::vector<cv::Point2f>& previous_tracked_features,
                        const std::vector<cv::Point2f>& current_tracked_features,
                        cv::Mat& essential_matrix_corner_mask,
                        Eigen::Matrix<double, 3, 1>& rotation_camera_coord);

  /**
   * @brief Optional helper function used to visualize the images and the feature tracks used
   *        to estimate the rotational ego-motion
   * 
   * @param[in] image                           // Camera image
   * @param[in] previous_tracked_features       // Vector of previous image's detected corner 
   *                                            //   features that were successfully tracked in
   *                                            //   the current frame
   * @param[in] current_tracked_features        // Vector of current image locations of features
   *                                            //   tracked from the previous frame
   * @param[in] seq_id                          // Image message sequence ID used to label saved
   *                                            //   images
   * @param[in] essential_matrix_corner_mask    // "Boolean" [CV_8UC1] mask that designates which
   *                                            //   tracked feature pairs were considered inliers
   *                                            //   for the essential matrix calculation
   * @param[in] write_to_disk                   // Boolean flag that, if true, writes the
   *                                            //  visualizer images to disk. Default: false.
   */
  void Visualize(const cv::Mat& image,
                 const std::vector<cv::Point2f>& previous_tracked_features,
                 const std::vector<cv::Point2f>& current_tracked_features,
                 const uint seq_id,
                 const cv::Mat& essential_matrix_corner_mask,
                 const bool write_to_disk = false);

  const uint max_num_features_;                   //  Maximum number of image corner features to
                                                  //   extract
  const uint min_feature_pixel_distance_;         // Minimum distance [pixels] between extracted
                                                  //   features
  const double min_feature_quality_level_;        // Minimum quality level of extracted features
                                                  //   [pu] See cv::goodFeaturesToTrack(...)
  const uint num_image_row_sections_;             // Number of image section rows
  const uint num_image_col_sections_;             // Number of image section columns
  const uint num_image_sections_;                 // Total number of image sections
  const uint max_num_section_features_;           // Maximum number of image corner features to
                                                  //   extract per image section
  const uint visible_hood_edge_top_v_;            // Image coordinate (v) that marks the (image)
                                                  //   top of the visible vehicle hood
  const uint visible_hood_edge_side_v_;           // Image coordinate (v) that marks the location
                                                  //   that the side of the vehicle hood leaves the
                                                  //   image
  const double essential_matrix_probability_;     // Desired level of confidence (probability)
                                                  //   that the essential matrix is correct. See
                                                  //   cv::findEssentialMat(...)
  const double essential_matrix_ransac_pixel_threshold_;
                                                  // Threshold distance [pixels] between feature
                                                  //   and its estimated epipolar line to be
                                                  //   considered a static inlier
  const uint min_num_of_tracks_;                  // Minimum number of feature tracks required to
                                                  //   confidently report an ego-motion estimate
  bool camera_info_set_;                          // Boolean flag that is true if the camera info
                                                  //   (intrinsics) are set
  cv::Mat camera_intrinsic_matrix_;               // 3x3 camera intrinsic matrix
  uint image_width_;                              // Image width
  uint image_height_;                             // Image height
  uint section_width_;                            // Image section width [pixels]
  uint section_height_;                           // Image section height [pixels]
  cv::Rect feature_tracking_rect_;                // Rectangle that describes image area for which
                                                  //   to track image features
  std::deque<cv::Mat> grayscale_images_;          // Double-ended queue that stores grayscale
                                                  //   images in the order they were received
  std::deque< std::vector<cv::Point2f> > detected_features_;
                                                  // Double-ended queue (corresponding to
                                                  //   the grayscale_images_ queue) that stores
                                                  //   the vector of detected image features
  double previous_msg_timestamp_;                 // Previous message's (image's) timestamp [sec.]
};
