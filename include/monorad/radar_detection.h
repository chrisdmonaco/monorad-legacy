/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

typedef unsigned int uint;

/**
 * @brief Class for storing a RADAR Detection (i.e. target)
 * 
 */
class RadarDetection {
 public:
  /**
   * @brief Construct a new RadarDetection object
   * 
   * @param[in] _theta          Measured azimuthal angle of radar detection [rad], pos. from
   *                              longitudinal axis to the left (CCW from bird's eye view)
   * @param[in] _range_rate     Measured range rate of radar detection [m/s]
   * @param[in] _scan_type      Delphi ESR RADAR Scan Type. 1 if mid-range, 2 if long-range
   */
  RadarDetection(const double _theta,
                 const double _range_rate,
                 const uint _scan_type)
  : theta(_theta),
    range_rate(_range_rate),
    scan_type(_scan_type),
    is_static_(false) { }

  /**
   * @brief Marks a target as being part of the static environment
   * 
   */
  inline void MarkStatic() { is_static_ = true; }

  /**
   * @brief Returns the static vs. dynamic state of the target
   * 
   * @return true    If static
   * @return false   Otherwise
   */
  inline bool is_static() const { return is_static_; }

  const double theta;         // Target azimuthal angle [rad], measured CCW from bird's eye view
  const double range_rate;    // Target range rate [m/s]
  const uint scan_type;       // Target scan type (1 for mid range, 2 for long range)

 private:
  bool is_static_;            // True if target has been marked as static (as opposed to
                              //   being part of a dynamic agent in the environment)
};
