/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <thread>
#include <functional>
#include <algorithm>
#include <iomanip>

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "monorad/rot_vel_estimator.h"

RotVelEstimator::RotVelEstimator(const uint max_num_features,
                                 const uint min_feature_pixel_distance,
                                 const double min_feature_quality_level,
                                 const uint num_image_row_sections,
                                 const uint num_image_col_sections,
                                 const uint visible_hood_edge_top_v,
                                 const uint visible_hood_edge_side_v,
                                 const double essential_matrix_probability,
                                 const double essential_matrix_ransac_pixel_threshold,
                                 const uint min_num_of_tracks)
  : max_num_features_(max_num_features),
    min_feature_pixel_distance_(min_feature_pixel_distance),
    min_feature_quality_level_(min_feature_quality_level),
    num_image_row_sections_(num_image_row_sections),
    num_image_col_sections_(num_image_col_sections),
    num_image_sections_(num_image_row_sections * num_image_col_sections),
    max_num_section_features_(max_num_features / num_image_sections_),
    visible_hood_edge_top_v_(visible_hood_edge_top_v),
    visible_hood_edge_side_v_(visible_hood_edge_side_v),
    essential_matrix_probability_(essential_matrix_probability),
    essential_matrix_ransac_pixel_threshold_(essential_matrix_ransac_pixel_threshold),
    min_num_of_tracks_(min_num_of_tracks),
    camera_info_set_(false),
    camera_intrinsic_matrix_(cv::Mat::eye(3, 3, CV_64F)) { }

void RotVelEstimator::SetCameraInfo(const double fx,
                                    const double fy,
                                    const double cx,
                                    const double cy,
                                    const uint image_width,
                                    const uint image_height) {
  // If camera info was already set, return. Assumes that camera info does not change
  // during execution.
  if (camera_info_set_) return;

  // Sets corresponding parameters in the stored intrinsic matrix
  camera_intrinsic_matrix_.at<double>(0) = fx;
  camera_intrinsic_matrix_.at<double>(4) = fy;
  camera_intrinsic_matrix_.at<double>(2) = cx;
  camera_intrinsic_matrix_.at<double>(5) = cy;

  image_width_ = image_width;
  image_height_ = image_height;

  // Calculates the image section width and heights. Width is straightfoward,
  // but section height calculation takes into account the image u-coordinate
  // of the vehicle hood's (image) top edge. In other words, ensures that the section
  // does not include any visible part of the vehicle's hood. Used later for image
  // feature detection.
  section_width_ = image_width / num_image_col_sections_;
  section_height_ = (visible_hood_edge_top_v_ + 1) < image_height ?
                    (visible_hood_edge_top_v_ + 1) / num_image_row_sections_ :
                    image_height / num_image_row_sections_;

  // Calculates the rectangle that defines the *entire* image area that can be used for image
  // feature tracking. Note that this is not defined by sections and it includes a bigger
  // area than the valid region for image detection. It includes *any* row that observes
  // the environment and not the visible hood. Therefore, it utilizes the image u-coordinate
  // where the side of the visible vehicle hood leaves the image.
  feature_tracking_rect_ =
    (visible_hood_edge_side_v_ + 1) > num_image_row_sections_ * section_height_ ?
    cv::Rect(0, 0, image_width, visible_hood_edge_side_v_ + 1) :
    cv::Rect(0, 0, image_width, num_image_row_sections_ * section_height_);

  camera_info_set_ = true;
}

bool RotVelEstimator::Estimate(const cv::Mat& grayscale_image,
                               const double timestamp,
                               const uint seq_id,
                               Eigen::Matrix<double, 3, 1>& rot_vel_camera_coord) {
  // If the camera info was not yet set, disregard image measurement until camera info is set
  if (!camera_info_set_) return false;

  // Push current grayscale image to front of double-ended queue. Also, create an empty
  // a corresponding detected_features_ vector in front of its double-ended queue.
  grayscale_images_.push_front(grayscale_image);
  detected_features_.emplace_front();

  // Extract image corner features from the current grayscale image and put them in its
  // corresponding detected features vector. 0 index corresponds to current measurement index.
  ExtractFeatures(grayscale_images_[0], detected_features_[0]);

  // Assume an invalid estimate until proven otherwise. Only proceed with estimation if more than
  // one image+features are stored, as a two consecutive are required for a valid estimate.
  bool valid_estimate = false;
  if (grayscale_images_.size() > 1 && detected_features_.size() > 1) {
    // Track detected features from the previous frame (index of 1) in the current image. Output
    // valid tracked feature pairs
    std::vector<cv::Point2f> previous_tracked_features;
    std::vector<cv::Point2f> current_tracked_features;
    TrackFeatures(grayscale_images_[1],
                  grayscale_images_[0],
                  detected_features_[1],
                  previous_tracked_features,
                  current_tracked_features);

    // If more than the minimum number of feature tracks are found and the time between images
    // is positive, continue to estimate the camera's rotation between images.
    const double delta_t = timestamp - previous_msg_timestamp_;
    if (current_tracked_features.size() >= min_num_of_tracks_ && delta_t > 0) {
      cv::Mat essential_matrix_corner_mask;
      Eigen::Vector3d rotation_camera_coord;
      EstimateRotation(previous_tracked_features,
                       current_tracked_features,
                       essential_matrix_corner_mask,
                       rotation_camera_coord);

      // Divide the camera's rotation between images (in the camera's coordinate system using
      // camera coordinate system conventions) by the time between images to yield its
      // corresponding rotational ego-motion. Outputs estimate in this convention.
      rot_vel_camera_coord = rotation_camera_coord / delta_t;

      /*
      Visualize(grayscale_image,
                previous_tracked_features,
                current_tracked_features,
                seq_id,
                essential_matrix_corner_mask,
                false);
      */

      valid_estimate = true;
    }

    // Remove the previous images+features in the double-ended queue as they are no longer needed
    grayscale_images_.pop_back();
    detected_features_.pop_back();
  }

  previous_msg_timestamp_ = timestamp;

  return valid_estimate;
}

void RotVelEstimator::ExtractFeatures(const cv::Mat& grayscale_image,
                                      std::vector<cv::Point2f>& extracted_features) {
  // Vector that holds a vector of image points for each image section.
  std::vector< std::vector<cv::Point2f> > image_sections(num_image_sections_);

  // Lambda function for extracting image features within an image subsection
  // Takes the image section index as an input. Image sections are labeled in row-order.
  auto ExtractImageSectionFeatures = [&](const uint section_idx) {
    // Reserve room for up to the maximum number of section image features
    image_sections[section_idx].reserve(max_num_section_features_);

    // Calculate row and column indexes from the row-order section index
    const uint section_row_idx = section_idx / num_image_col_sections_;
    const uint section_col_idx = section_idx % num_image_col_sections_;

    // Calculate which part of the image corresponds to this image section. This rectangle
    // is used for feature extraction.
    const cv::Rect feature_extraction_rect(section_col_idx * section_width_,
                                           section_row_idx * section_height_,
                                           section_width_,
                                           section_height_);

    // Extract so-called ``good feature to track". Only pass through the data corresponding to the
    // image section (in lieu of a mask). Features are stored in their image section's
    // corresponding location in the vector.
    cv::goodFeaturesToTrack(grayscale_image(feature_extraction_rect),
                            image_sections[section_idx],
                            max_num_section_features_,
                            min_feature_quality_level_,
                            min_feature_pixel_distance_);

    // Since the features were detected assuming the image section was the entire image, add the
    // feature extraction rectangle's top-left corner to ensure they represent the coordinates
    // for the entire image
    const cv::Point2f feature_extraction_rect_tl = feature_extraction_rect.tl();
    for (cv::Point2f& feature : image_sections[section_idx]) {
      feature += feature_extraction_rect_tl;
    }
  };

  // Assign a thread for each of the image sections. For each thread/section, extract its
  // image features. Then, ensure all threads have completed their processes before
  // continuing.
  std::vector<std::thread> image_section_threads;
  for (uint section_idx = 0; section_idx < num_image_sections_; ++section_idx) {
    image_section_threads.emplace_back(ExtractImageSectionFeatures, section_idx);
  }
  for (std::thread& thread : image_section_threads) thread.join();

  // Clear the previously extracted features. For each image section, move its extracted image
  // features into the entire image's extracted_features vector.
  extracted_features.clear();
  extracted_features.reserve(max_num_features_);
  for (std::vector<cv::Point2f>& section_features : image_sections) {
    extracted_features.insert(extracted_features.end(),
                              std::make_move_iterator(section_features.begin()),
                              std::make_move_iterator(section_features.end()));
  }
}

void RotVelEstimator::TrackFeatures(const cv::Mat& previous_grayscale_image,
                                    const cv::Mat& current_grayscale_image,
                                    std::vector<cv::Point2f>& previous_detected_features,
                                    std::vector<cv::Point2f>& previous_tracked_features,
                                    std::vector<cv::Point2f>& current_tracked_features) {
  // Create a vector for the unfiltered tracked features. It needs to be as big as the
  // previously detected features vector. Do the same for the statuses and errors vectors.

  std::vector<cv::Point2f> unfiltered_tracked_features;
  unfiltered_tracked_features.reserve(previous_detected_features.size());

  std::vector<uchar> statuses;
  std::vector<float> errors;
  statuses.reserve(previous_detected_features.size());
  errors.reserve(previous_detected_features.size());

  // Track previously detected features between the previous and current grayscale image
  // via Lucas-Kanade optical flow. Only the image portion corresponding to the feature
  // tracking rectangle is passed through. Since this rectangle's top-left corner is (0, 0),
  // no further image coordinate adjustments are necessary. Note that unfiltered_tracked_features
  // contains all ``tracked" features, even if they were successfully tracked or not. Hence,
  // it is ``unfiltered." The statuses vector indicates successful tracks.
  cv::calcOpticalFlowPyrLK(previous_grayscale_image(feature_tracking_rect_),
                           current_grayscale_image(feature_tracking_rect_),
                           previous_detected_features, unfiltered_tracked_features,
                           statuses, errors);

  previous_tracked_features.clear();
  current_tracked_features.clear();

  previous_tracked_features.reserve(statuses.size());
  current_tracked_features.reserve(statuses.size());

  // If the corresponding statuses vector value is true, that feature pair was successfully
  // tracked. Thus, add it (move it) to the previous_tracked_features and current_tracked_features.
  //  Caution: this moving process changes the input previous_detected_features vector, putting it
  // in an indeterminate state.
  for (uint point_idx = 0; point_idx < statuses.size(); ++point_idx) {
    if (statuses[point_idx]) {
      previous_tracked_features.push_back(std::move(previous_detected_features[point_idx]));
      current_tracked_features.push_back(std::move(unfiltered_tracked_features[point_idx]));
    }
  }
}

void RotVelEstimator::EstimateRotation(const std::vector<cv::Point2f>& previous_tracked_features,
                                       const std::vector<cv::Point2f>& current_tracked_features,
                                       cv::Mat& essential_matrix_corner_mask,
                                      Eigen::Matrix<double, 3, 1>& rotation_camera_coord) {
  // Estimate the essential matrix using the tracked features between the previous and current
  // image. Uses RANSAC to reject outliers (e.g. dynamic agents in the environment).
  const cv::Mat essential_matrix = cv::findEssentialMat(previous_tracked_features,
                                                        current_tracked_features,
                                                        camera_intrinsic_matrix_,
                                                        cv::RANSAC,
                                                        essential_matrix_probability_,
                                                        essential_matrix_ransac_pixel_threshold_,
                                                        essential_matrix_corner_mask);

  // Decomposes the essential matrix into its two possible rotation matrix possibilities
  // (and +/- translation unit vector)
  cv::Mat rotation_matrix_1;
  cv::Mat rotation_matrix_2;
  cv::Mat translation_unit_vector;
  cv::decomposeEssentialMat(essential_matrix,
                            rotation_matrix_1,
                            rotation_matrix_2,
                            translation_unit_vector);

  // Convert each rotation matrix into its corresponding Euler rotation vector. Note that
  // OpenCV reports its rotation matrices with respect to observed points. Thus, the rotation
  // matrix transpose must be taken to describe the rotation from the (k-1) to (k) camera
  // pose
  cv::Mat rotation_vector_1;
  cv::Mat rotation_vector_2;
  cv::Rodrigues(rotation_matrix_1.t(), rotation_vector_1);
  cv::Rodrigues(rotation_matrix_2.t(), rotation_vector_2);

  // Due to vehicle motion constraints, only the rotation vector corresponding to the smallest
  // vehicle yaw change is valid. This is the rotation vector that will be used for the rotational
  // ego-motion estimate. Since the rotation vectors are described according to the
  // camera coordinate system conventions, this corresponds to the camera's y-axis. This assumes
  // that the camera's y-axis is *mostly* vertical.
  const cv::Mat rotation_vector =
    std::abs(rotation_vector_1.at<double>(1)) <  std::abs(rotation_vector_2.at<double>(1)) ?
    rotation_vector_1 : rotation_vector_2;

  // Output the elements of the valid rotation vector [rad]
  rotation_camera_coord << rotation_vector.at<double>(0),
                           rotation_vector.at<double>(1),
                           rotation_vector.at<double>(2);
}

void RotVelEstimator::Visualize(const cv::Mat& image,
                                const std::vector<cv::Point2f>& previous_tracked_features,
                                const std::vector<cv::Point2f>& current_tracked_features,
                                const uint seq_id,
                                const cv::Mat& essential_matrix_corner_mask,
                                const bool write_to_disk) {
  // Convert the grayscale image to color, as it is required for later visualization overlays
  cv::Mat display_image;
  cv::cvtColor(image, display_image, CV_GRAY2BGR);

  for (uint point_idx = 0; point_idx < current_tracked_features.size(); ++point_idx) {
    // If a feature track is an inlier for the essential matrix, draw the current feature
    // with a blue circle and its feature optical flow track in green
    if (essential_matrix_corner_mask.at<uchar>(point_idx))  {
      cv::line(display_image,
               previous_tracked_features[point_idx],
               current_tracked_features[point_idx],
               cv::Scalar(0, 255, 0),
               2);
      cv::circle(display_image,
                 current_tracked_features[point_idx],
                 3,
                 cv::Scalar(255, 0, 0));
    } else {  // Otherwise, draw both in red
      cv::line(display_image,
               previous_tracked_features[point_idx],
               current_tracked_features[point_idx],
               cv::Scalar(0, 0, 255),
               1);
      cv::circle(display_image,
                 current_tracked_features[point_idx],
                 1,
                 cv::Scalar(0, 0, 255));
    }
  }

  // If option is set to write to disk, save resulting visualization image with its sequence ID
  // as the filename
  if (write_to_disk) {
    std::ostringstream image_filename;
    image_filename << std::setfill('0') << std::setw(5) <<  seq_id;
    cv::imwrite(image_filename.str() + ".png", display_image);
  } else {  // Otherwise, just display it in a window
    cv::namedWindow("Rot Vel Visualizer");
    cv::imshow("Rot Vel Visualizer", display_image);
    cv::waitKey(1);
  }
}
