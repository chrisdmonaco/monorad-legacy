/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include "ros/ros.h"

#include "monorad/monorad.h"

/**
 * @brief Starts the "MonoRAD" ROS node.
 * 
 */
int main(int argc, char* argv[]) {
  ros::init(argc, argv, "monorad");

  MonoRAD monorad;

  ros::spin();

  return 0;
}
