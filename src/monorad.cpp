/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include "cv_bridge/cv_bridge.h"

#include "monorad/monorad.h"

/*********************************USER-DEFINED PRE-SET PARAMETERS********************************/
static constexpr uint kTransVelRANSACIterations = 15;   // Iterations for trans. vel. estimation
                                                        //   RANSAC inlier extraction
static constexpr uint kMaxNumTransVelOptimizationIterations = 10;
                                                        // Max number of iterations for trans. vel.
                                                        //   estimation non-linear optimization
static constexpr bool kVerboseTransVelOptimization = false;
                                                        // Flag to make trans. vel. optimization
                                                        //   verbose while optimizing
static constexpr double kRangeRateSigmaMetersPerSec = 0.12;
                                                        // RADAR extracted target range rate
                                                        //   measurement standard deviation [m/s]
static constexpr double kRADARLongRangeThetaSigmaDeg = 0.5;
                                                        // RADAR extracted target azimuthal angle
                                                        //  standard deviation [deg] for long-range
                                                        //  scan
static constexpr double kRADARMidRangeThetaSigmaDeg = 1.0;
                                                        // RADAR extracted target azimuthal angle
                                                        //  standard deviation [deg] for mid-range
                                                        //  scans
static constexpr uint kMaxNumRADARTargetsPerScan = 64;  // Maximum number of returned RADAR targets
                                                        //   per mid- or long- range scan
static constexpr uint kMaxImageFeatures = 180;          // Maximum number of image features to
                                                        //   extract from the entire image
static constexpr uint kMinFeaturePixelDistance = 10;    // Minimum pixel distance between extracted
                                                        //   features
static constexpr double kMinFeatureQualityLevel = 0.0001;
                                                        // Minimum quality level of extracted image
                                                        //   features
static constexpr uint kNumImageRowSections = 2;         // Number of image section rows
static constexpr uint kNumImageColSections = 3;         // Number of image section columns
static constexpr uint kVisibleHoodEdgeTopV = 567;       // Image v-coordinate of the (image) top of
                                                        //   the visible hood
static constexpr uint kVisibleHoodEdgeSideV = 750;      // Image v-coordinate where the vehicle hood
                                                        //   edge leaves the image
static constexpr double kEssentialMatrixProbability = 0.99;
                                                        // Level of confidence (probability) that
                                                        //   the calculated essential matrix is
                                                        //   correct
static constexpr double kEssentialMatrixRANSACPixelThreshold = 0.15;
                                                        // Distance threshold [pixels] between an
                                                        //   image feature and its estimated
                                                        //   epipolar line to be considered a
                                                        //   static inlier
static constexpr uint kMinNumOfTracks = 10;             // Minimum number of image feature tracks
                                                        //   to confidently estimate ego-motion

/***********************************PRE-CALCULATED PARAMETERS*************************************/
static constexpr double kRangeRateRANSACInlierThreshold = 2.0 * kRangeRateSigmaMetersPerSec;
static constexpr double kRADARLongRangeThetaSigmaRad = kRADARLongRangeThetaSigmaDeg * M_PI / 180.0;
static constexpr double kRADARMidRangeThetaSigmaRad = kRADARMidRangeThetaSigmaDeg * M_PI / 180.0;
static constexpr double kRADARMedianThetaSigmaRad =
  (kRADARLongRangeThetaSigmaRad + kRADARMidRangeThetaSigmaRad) / 2.0;

MonoRAD::MonoRAD()
  : mid_range_scan_stored_(false),
    long_range_scan_stored_(false),
    body_coord_R_camera_coord_(
      (Eigen::Matrix<double, 3, 3>() << 0, 0, 1, -1, 0, 0, 0, -1, 0).finished()),
    trans_vel_estimator_(kTransVelRANSACIterations,
                         kRangeRateRANSACInlierThreshold,
                         kRangeRateSigmaMetersPerSec,
                         kRADARMedianThetaSigmaRad,
                         kMaxNumTransVelOptimizationIterations,
                         kVerboseTransVelOptimization),
    rot_vel_estimator_(kMaxImageFeatures,
                       kMinFeaturePixelDistance,
                       kMinFeatureQualityLevel,
                       kNumImageRowSections,
                       kNumImageColSections,
                       kVisibleHoodEdgeTopV,
                       kVisibleHoodEdgeSideV,
                       kEssentialMatrixProbability,
                       kEssentialMatrixRANSACPixelThreshold,
                       kMinNumOfTracks) {
  radar_data_subscriber_ = nh_.subscribe(
    "/parsed_tx/esr_eth_tx_msg",
    4,
    &MonoRAD::RadarDataCallback,
    this);

  camera_info_subscriber_ = nh_.subscribe(
    "/camera/camera_info",
    1,
    &MonoRAD::CameraInfoCallback,
    this);

  camera_image_subscriber_ = nh_.subscribe(
    "/camera/image_rect/compressed",
    2,
    &MonoRAD::CameraImageCallback,
    this);

  radar_egomotion_publisher_ =
    nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>("/radar_egomotion", 2);

  camera_egomotion_publisher_ =
    nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>("/camera_egomotion", 2);

  // Reserve the RadarDetection vector to be big enough for all targets if both the
  // mid- and long-range scans return their maximum number of targets
  valid_radar_detections_.reserve(2 * kMaxNumRADARTargetsPerScan);
}

bool MonoRAD::StoreRadarData(const delphi_esr_msgs::EsrEthTx& radar_data_msg) {
  // Mark if this scan's data corresponds to the mid-range or long-range scan. They are published
  // as separate message instances, but should be analyzed together.
  switch (radar_data_msg.xcp_scan_type) {
    case 1:
      mid_range_scan_stored_ = true;
      break;
    case 2:
      long_range_scan_stored_ = true;
      break;
  }

  // Iterate through all reported targets for the scan
  for (uint target_idx = 0;
      target_idx < radar_data_msg.target_report_range.size();
      ++target_idx) {
    // Only valid targets have a non-zero range
    if (!radar_data_msg.target_report_range[target_idx]) continue;

    // Stores the azimuthal angle (converted to rad), range rate,
    // and scan type of valid detected target
    valid_radar_detections_.emplace_back(
      (radar_data_msg.target_report_theta[target_idx]) * M_PI / 180.0,
      radar_data_msg.target_report_range_rate[target_idx],
      radar_data_msg.xcp_scan_type);
  }

  // Return true only if data from both RADAR scans are stored
  return (mid_range_scan_stored_ && long_range_scan_stored_) ? true : false;
}

void MonoRAD::RadarDataCallback(
    const delphi_esr_msgs::EsrEthTxConstPtr radar_data_msg_ptr) {
  // Only estimate translational ego-motion if data from both the mid- and long-range scans
  // have been stored
  if (StoreRadarData(*radar_data_msg_ptr)) {
    Eigen::Vector2d radar_trans_vel_estimate;
    Eigen::Matrix<double, 2, 2> radar_trans_vel_covariance_matrix;

    // If there are more than three valid radar detections, try to estimate the RADAR's
    // translational velocity. If this returns true, indicating that a valid estimate has been
    // calculated, then publish this ego-motion estimate
    if (valid_radar_detections_.size() > 3 &&
        trans_vel_estimator_.Estimate(valid_radar_detections_,
                                      radar_trans_vel_estimate,
                                      radar_trans_vel_covariance_matrix)) {
      PublishEgomotionEstimate(radar_trans_vel_estimate,
                               radar_trans_vel_covariance_matrix,
                               radar_data_msg_ptr->header);
    }

    // Reset data storage parameters so that it can accept a new measurement
    mid_range_scan_stored_ = false;
    long_range_scan_stored_ = false;
    valid_radar_detections_.clear();
  }
}

void MonoRAD::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr camera_info_msg_ptr) {
  // Set the stored camera intrinsic parameters using the received CameraInfo message
  rot_vel_estimator_.SetCameraInfo(camera_info_msg_ptr->K[0],  // fx
                                   camera_info_msg_ptr->K[4],  // fy
                                   camera_info_msg_ptr->K[2],  // cx
                                   camera_info_msg_ptr->K[5],  // cy
                                   camera_info_msg_ptr->width,
                                   camera_info_msg_ptr->height);
}

void MonoRAD::CameraImageCallback(const sensor_msgs::CompressedImageConstPtr image_msg_ptr) {
  // Converts grayscale image from ROS message to OpenCV format. Copying is required to use it
  // during the next iteration
  const cv::Mat grayscale_image = cv_bridge::toCvCopy(image_msg_ptr, "mono8")->image;

  // Only proceed if the rotational velocity estimator returns true, indicating that a valid
  // estimate was calculated. Otherwise, the end of the callback function will be reached and
  // it will wait for the next image.
  Eigen::Vector3d rot_vel_camera_coord;
  if (rot_vel_estimator_.Estimate(grayscale_image,
                                   image_msg_ptr->header.stamp.toSec(),
                                   image_msg_ptr->header.seq,
                                   rot_vel_camera_coord)) {
    // Since a valid estimate has been calculated, convert this estimate (in the camera's
    // coordinate system using camera coordinate system conventions) into the robotics vehicle
    // body coordinate system (and corresponding conventions). Note that this is general and does
    // not account for extrinsic parameters.
    const Eigen::Vector3d rot_vel_body_coord = body_coord_R_camera_coord_ * rot_vel_camera_coord;

    // Publish ego-motion estimate. Note that covariance estimation has not yet been developed.
    PublishEgomotionEstimate(rot_vel_body_coord,
                             image_msg_ptr->header);
  }
}

void MonoRAD::PublishEgomotionEstimate(
    const Eigen::Vector2d& radar_trans_vel_estimate,
    const Eigen::Matrix<double, 2, 2>& radar_trans_vel_covariance_matrix,
    const std_msgs::Header& msg_header) {
  // Use message header from source measurement's message
  radar_egomotion_msg_.header = msg_header;

  radar_egomotion_msg_.twist.twist.linear.x = radar_trans_vel_estimate[0];
  radar_egomotion_msg_.twist.twist.linear.y = radar_trans_vel_estimate[1];

  // Publish estimate covariances by mapping the RADAR's 3x3 translational velocity
  // covariance matrix into the "twist" message's full 6x6 motion state covariance
  // matrix
  radar_egomotion_msg_.twist.covariance[0] = radar_trans_vel_covariance_matrix(0);
  radar_egomotion_msg_.twist.covariance[1] = radar_trans_vel_covariance_matrix(1);
  radar_egomotion_msg_.twist.covariance[6] = radar_trans_vel_covariance_matrix(2);
  radar_egomotion_msg_.twist.covariance[7] = radar_trans_vel_covariance_matrix(3);
  radar_egomotion_msg_.twist.covariance[14] = -1;  // indicates that it is unknown

  radar_egomotion_publisher_.publish(radar_egomotion_msg_);
}

void MonoRAD::PublishEgomotionEstimate(
    const Eigen::Vector3d& camera_rot_vel_estimate_body_coord,
    const std_msgs::Header& msg_header) {
  // Use message header from source measurement's message
  camera_egomotion_msg_.header = msg_header;

  camera_egomotion_msg_.twist.twist.angular.x = camera_rot_vel_estimate_body_coord[0];
  camera_egomotion_msg_.twist.twist.angular.y = camera_rot_vel_estimate_body_coord[1];
  camera_egomotion_msg_.twist.twist.angular.z = camera_rot_vel_estimate_body_coord[2];

  camera_egomotion_publisher_.publish(camera_egomotion_msg_);
}
